import pandas as pd
from statsmodels.multivariate.manova import MANOVA

df = pd.read_csv('data/trajectories.csv')
columns = df.columns
columns = [i for i in columns if '/' in i]
model = MANOVA(endog=columns, exog='session')
#fit = moldel.fit(df)
