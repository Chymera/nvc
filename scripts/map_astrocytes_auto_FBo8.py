import samri.plotting.maps as maps

stat_map = "data/l1_astrocytes/sub-FBo8/ses-ketxyl/sub-FBo8_ses-ketxyl_task-lhp_acq-geEPI_run-0_bold_tstat.nii.gz"
template = "/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii"

maps.stat3D(stat_map,
	scale=0.2,
	template=template,
	show_plot=False,
	threshold=2.5,
	)
