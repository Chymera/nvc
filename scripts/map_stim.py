import samri.plotting.maps as maps

stat_map = "data/l2_arcabeta/acq-geEPI_run-0_tstat.nii.gz"
template = "/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii"

maps.stat3D(stat_map,
	cut_coords=(1.7,-1.4,-0.7),
	scale=0.2,
	template=template,
	show_plot=False,
	threshold=1.5,
	)
