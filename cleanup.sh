#!/usr/bin/env bash

rm -rf auto_fig_py*.png *.aux *.bbl *.blg *.log _minted-slides *.nav *.out *.pgf pythontex-files-* *.pytxcode *.snm *.toc *.vrb
