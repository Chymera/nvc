import pandas as pd
from itertools import product
from samri.utilities import bids_autofind_df

bids_df = bids_autofind_df('~/.scratch/ztau/roi_correlations/',
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-rest_acq-EPI_run-{{run}}_correlation.csv',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_acq-(?P<acquisition>.+)_run-(?P<run>.+)_correlation\.csv',
	)

whitelist = [
	'amygdala (R)',
	'amygdala (L)',
	'Dorsal intermediate entorhinal cortex (R)',
	'Dorsal intermediate entorhinal cortex (L)',
	'Dorsolateral entorhinal cortex (R)',
	'Dorsolateral entorhinal cortex (L)',
	'Medial entorhinal cortex (R)',
	'Medial entorhinal cortex (L)',
	'Caudomedial entorhinal cortex (R)',
	'Caudomedial entorhinal cortex (L)',
	'Ventral intermediate entorhinal cortex (R)',
	'Ventral intermediate entorhinal cortex (L)',
	'CA10r (R)',
	'CA10r (L)',
	'LMol (R)',
	'LMol (L)',
	'CA1Rad (R)',
	'CA1Rad (L)',
	'CA2Py (R)',
	'CA2Py (L)',
	'CA20r (R)',
	'CA20r (L)',
	'CA2Rad (R)',
	'CA2Rad (L)',
	'CA3Py Inner (R)',
	'CA3Py Inner (L)',
	'CA3Py Outer (R)',
	'CA3Py Outer (L)',
	'CA30r (R)',
	'CA30r (L)',
	'CA3Rad (R)',
	'CA3Rad (L)',
	'SLu (R)',
	'SLu (L)',
	'MoDG right (R)',
	'MoDG right (L)',
	'GrDG (R)',
	'GrDG (L)',
	'PoDG (R)',
	'PoDG (L)',
	'CA1Py (R)',
	'CA1Py (L)',
	]

for index, row in bids_df.iterrows():
	path = row['path']
	df = pd.read_csv(path, index_col=0)
	df = df[whitelist]
	df = df.ix[whitelist]
	for a, b in product (whitelist, whitelist):
		if a == b:
			continue
		else:
			val = df[a].ix[b]
			name = '{}/{}'.format(a,b)
		bids_df.loc[bids_df['path']==path,name] = val

bids_df.to_csv('../data/trajectories.csv')
