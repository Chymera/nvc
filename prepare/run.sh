#!/usr/bin/env bash

#if [ ! -d ~/.scratch ]; then
#        echo "You seem to be lacking a ~/.scratch/ directory."
#        echo "We need this directory in order to process the data, and it needs to be on a volume with 200GB+ space."
#        echo "You can simply symlink to a location you would like this to happen (and then re-run this script):
#                ln -s /where/you/want/it ~/.scratch"
#        exit 1
#fi
#
#if [ ! -d ~/.scratch/nvc/bids ]; then
#        if [ -d "/usr/share/nvc_bidsdata" ]; then
#                [ -d ~/.scratch/nvc ] || mkdir ~/.scratch/nvc
#                ln -s "/usr/share/nvc_bidsdata" ~/.scratch/nvc/bids
#        else
#                echo "No OPFVTA BIDS data distribution found, processing from scanner OPFVTA data:"
#                [ -d ~/.scratch/nvc ] || mkdir ~/.scratch/nvc
#		SAMRI bru2bids -o ~/.scratch/nvc/\
#			-f '{"acquisition":["geEPI"]}'\
#			-s '{"acquisition":["TurboRARE"]}'\
#			/mnt/data/nvc/
#		rsync -avP /mnt/data/nvc/participants.tsv ~/.scratch/nvc/
#        fi
#fi
#
#python preprocess.py || exit 1
#python manual_overview.py || exit 1
#python l1.py || exit 1
#python l2.py || exit 1
rsync -avP --exclude='*_cope.nii*' --exclude='*_zstat.nii*' ~/.scratch/nvc/*l2* ../data/ || exit 1
rsync -avP\
	--exclude='*_cope.nii*'\
	--exclude='*_zstat.nii*'\
	--exclude='*_betas.nii*'\
	--exclude='*_varcb.nii*'\
	--exclude='*_pstat.nii*'\
	--exclude='*_pfstat.nii*'\
	--exclude='*_bold.nii*'\
	--exclude='*_design.mat*'\
	--exclude='*_design.png*'\
	--exclude='*_work*'\
	~/.scratch/nvc/*l1* ../data/ || exit 1
rsync -avP ~/.scratch/nvc/registration_detail/FBo{8,9}* ../data/registration_detail || exit 1
