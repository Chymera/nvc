from os import path
from samri.pipelines import glm

scratch_dir = '~/.scratch/nvc'

preprocess_base = path.join(scratch_dir,'preprocessed')

glm.l1(preprocess_base,
	highpass_sigma=180,
	habituation=None,
	mask='mouse',
	n_jobs_percentage=.33,
	match={'modality':['bold']},
	invert=False,
	workflow_name='l1',
	out_base=scratch_dir,
	keep_work=True,
	)
glm.l1_physio(preprocess_base,'astrocytes',
	highpass_sigma=180,
	convolution=False,
	mask='mouse',
	n_jobs_percentage=.33,
	match={
		'modality':['bold'],
		},
	exclude={
		'subject':['FBo1','FBo2'],
		},
	invert=False,
	workflow_name='l1_astrocytes',
	out_base=scratch_dir,
	)
glm.l1_physio(preprocess_base,'neurons',
	highpass_sigma=180,
	mask='mouse',
	n_jobs_percentage=.33,
	match={
		'modality':['bold'],
		},
	exclude={
		'subject':['FBo1','FBo2'],
		},
	invert=False,
	workflow_name='l1_neurons',
	out_base=scratch_dir,
	)

#glm.l1_physio(preprocess_base,'astrocytes',
#        highpass_sigma=180,
 #       convolution=True,
 #       mask='mouse',
  #      n_jobs_percentage=.33,
   #     match={
    #            'modality':['bold'],
     #           },
      #  exclude={
       #         'subject':['FBo1','FBo2'],
        #        },
        #invert=False,
       # workflow_name='l1_astrocytes_convolved',
       # out_base=scratch_dir,
	#)
