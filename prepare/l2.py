from os import path
from samri.pipelines import glm

scratch_dir = '~/.scratch/nvc'

glm.l2_common_effect('{}/l1/'.format(scratch_dir),
	subjects=['FBo4','FBo5','FBo8','FBo9',],
	workflow_name='l2_arcabeta',
	mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	groupby='none',
	include={'run':['0'],},
	n_jobs_percentage=.33,
	out_base=scratch_dir,
	run_mode='fe',
	)

glm.l2_common_effect('{}/l1_astrocytes/'.format(scratch_dir),
	subjects=['FBo4','FBo5','FBo8','FBo9',],
	workflow_name='l2_astrocytes_arcabeta',
	mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	groupby='none',
	include={'run':['0'],},
	n_jobs_percentage=.33,
	out_base=scratch_dir,
	run_mode='fe',
	)

glm.l2_common_effect('{}/l1_neurons/'.format(scratch_dir),
	subjects=['FBo4','FBo5','FBo8','FBo9',],
	workflow_name='l2_neurons_arcabeta',
	mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	groupby='none',
	include={'run':['0'],},
	n_jobs_percentage=.33,
	out_base=scratch_dir,
	run_mode='fe',
	)
