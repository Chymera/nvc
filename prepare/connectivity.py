from samri.utilities import bids_autofind_df, ordered_structures
from samri.analysis.fc import correlation_matrix

bids_df = bids_autofind_df('~/.scratch/ztau/preprocessing/',
	path_template='sub-{{subject}}/ses-{{session}}/func/'\
		'sub-{{subject}}_ses-{{session}}_task-rest_acq-EPI_run-{{run}}.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/func/'\
		'.*?_acq-(?P<acquisition>.+)_run-(?P<run>.+)\.nii\.gz',
	)

bids_df['out_path'] = bids_df['path'].str.replace('preprocessing','roi_correlations')
bids_df['out_path'] = bids_df['out_path'].str.replace('.nii.gz','_correlation.csv')
bids_df['out_path'] = bids_df['out_path'].str.replace('/func','')

structure_names = ordered_structures()
for index, row in bids_df.iterrows():
	correlation_matrix(row['path'],
		atlas='/usr/share/mouse-brain-atlases/dsurqec_40micron_labels.nii',
		save_as=row['out_path'],
		structure_names=structure_names,
		)
